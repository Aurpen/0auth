const { readFile } = require("fs").promises;

async function getCredentials(options) {
  const { credentialsPath, credentialsType } = options;
  if (!credentialsPath) throw `credentialsPath is undefined`;
  const type = credentialsType || "installed";
  try {
    const content = await readFile(credentialsPath);
    const credentials = JSON.parse(content);
    return credentials[type];
  } catch (error) {
    if (error instanceof ReferenceError) {
      throw error;
    } else {
      throw `${credentialsPath} not found`;
    }
  }
}

function getScopes(options) {
  const { scope } = options;
  if (!scope) throw `API_SCOPES is undefined`;
  return scope.split(",");
}

function getTokenPath(options) {
  const { tokenPath } = options;
  if (!tokenPath) throw `tokenPath is undefined`;
  return tokenPath;
}

async function getAuthParameters(options) {
  const credentials = await getCredentials(options);
  const scope = getScopes(options);
  const tokenPath = getTokenPath(options);
  return { scope, credentials, tokenPath };
}

module.exports = {
  getCredentials,
  getScopes,
  getTokenPath,
  getAuthParameters,
};
