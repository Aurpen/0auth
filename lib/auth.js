const fs = require("fs");
const readline = require("readline");
const { google } = require("googleapis");

class AuthManager {
  constructor({ scope, credentials, tokenPath }) {
    this.scope = scope;
    this.credentials = credentials;
    this.tokenPath = tokenPath;
  }

  authorize = () => {
    return new Promise(async (resolve) => {
      const { credentials } = this;
      const { client_secret, client_id, redirect_uris } = credentials;

      const oAuth2Client = new google.auth.OAuth2(
        client_id,
        client_secret,
        redirect_uris[0]
      );

      fs.readFile(this.tokenPath, (err, token) => {
        if (err) {
          this.getNewToken(oAuth2Client, resolve);
        } else {
          oAuth2Client.setCredentials(JSON.parse(token));
          resolve(oAuth2Client);
        }
      });
    });
  };

  getNewToken = (oAuth2Client, callback) => {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: "offline",
      scope: this.scope,
    });
    console.log("Authorize this app by visiting this url:", authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question("Enter the code from that page here: ", (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return console.error("Error retrieving access token", err);
        oAuth2Client.setCredentials(token);
        fs.writeFile(this.tokenPath, JSON.stringify(token), (err) => {
          if (err) return console.error(err);
          console.log("Token stored to", this.tokenPath);
        });
        callback(oAuth2Client);
      });
    });
  };
}

module.exports = AuthManager;
