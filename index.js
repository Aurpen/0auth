const AuthManager = require("./lib/auth");
const { getAuthParameters } = require("./lib/parameters");

async function Authorise(env) {
  const parameters = await getAuthParameters(env);
  const auth = new AuthManager(parameters);
  return auth.authorize();
}

module.exports = Authorise;
